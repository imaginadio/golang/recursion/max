package main

import (
	"fmt"
)

func main() {
	arr := []int{1, 2, 3, 4, 5, 4, 3, 2, 1}
	fmt.Println(MaxRecursiveRewrite(arr))
	fmt.Println(MaxRecursive(arr))
}

func MaxRecursive(arr []int) int {
	if len(arr) == 0 {
		return 0
	} else if len(arr) == 1 {
		return arr[0]
	} else if len(arr) == 2 {
		if arr[0] > arr[1] {
			return arr[0]
		} else {
			return arr[1]
		}
	} else {
		sub := MaxRecursive(arr[1:])
		if arr[0] > sub {
			return arr[0]
		} else {
			return sub
		}
	}
}

func MaxRecursiveRewrite(arr []int) int {
	if len(arr) == 0 {
		return 0
	} else if len(arr) == 1 {
		return arr[0]
	} else {
		if arr[0] > arr[1] {
			arr[1] = arr[0]
		}
		return MaxRecursiveRewrite(arr[1:])
	}
}
