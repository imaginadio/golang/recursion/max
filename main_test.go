package main

import "testing"

var arr = []int{1, 2, 3, 4, 5, 4, 3, 2, 1}

func BenchmarkMaxRecursive(t *testing.B) {
	for i := 0; i < t.N; i++ {
		MaxRecursive(arr)
	}
}

func BenchmarkMaxRecursiveRewrite(t *testing.B) {
	for i := 0; i < t.N; i++ {
		MaxRecursiveRewrite(arr)
	}
}
